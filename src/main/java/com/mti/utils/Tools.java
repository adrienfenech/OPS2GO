package com.mti.utils;

import java.math.BigInteger;
import java.security.SecureRandom;

public class Tools {
    public static String pageToTitle(Page p) {
        switch (p) {
            case DASHBOARD:
                return "Tableau de bord";
            case PILOTS:
                return "Pilotes";
            case MISSIONS:
                return "Missions";
            case PROFILE:
                return "Profil";
            default:
                return "";
        }
    }

    public static Page stringToPage(String str) {
        switch (str) {
            case "Tableau de bord":
                return Page.DASHBOARD;
            case "Pilotes":
                return Page.PILOTS;
            case "Missions":
                return Page.MISSIONS;
            case "Profil":
                return Page.PROFILE;
            default:
                return Page.NONE;
        }
    }

    public static String generateString() {
        SecureRandom random = new SecureRandom();
        return new BigInteger(130, random).toString(32);
    }
}
