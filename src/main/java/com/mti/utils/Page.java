package com.mti.utils;

public enum Page {
    DASHBOARD,
    MISSIONS,
    PILOTS,
    PROFILE,
    NONE,
}
