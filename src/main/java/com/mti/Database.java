package com.mti;

import com.mti.dao.*;
import com.mti.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sqlite.SQLiteConfig;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Jimmy on 22/11/15.
 */
public class Database {
    private static boolean isDbInit = false;

    private static final Logger Log = LogManager.getLogger(Database.class);

    public static final String DB_URL = "jdbc:sqlite:ops2go.db";
    public static final String DRIVER = "org.sqlite.JDBC";

    public static Connection getConnection() throws ClassNotFoundException {
        Class.forName(DRIVER);
        Connection connection = null;
        try {
            SQLiteConfig config = new SQLiteConfig();
            config.enforceForeignKeys(true);
            connection = DriverManager.getConnection(DB_URL,config.toProperties());
            connection.setAutoCommit(false);
        } catch (SQLException ex) {}
        return connection;
    }

    public synchronized static void initDb() {
        if (isDbInit)
            return;

        try {
            Connection c = getConnection();
            Statement stmt = c.createStatement();

            Log.info("Drop tables stated");
            stmt.execute("DROP TABLE IF EXISTS PILOTSKILL");
            stmt.execute("DROP TABLE IF EXISTS MANAGERTEAM");
            stmt.execute("DROP TABLE IF EXISTS MISSIONPILOTPLANE");
            stmt.execute("DROP TABLE IF EXISTS MANAGER");
            stmt.execute("DROP TABLE IF EXISTS MISSION");
            stmt.execute("DROP TABLE IF EXISTS SKILL");
            stmt.execute("DROP TABLE IF EXISTS PILOT");
            stmt.execute("DROP TABLE IF EXISTS PLANE");
            stmt.execute("DROP TABLE IF EXISTS TEAM");
            Log.info("Drop tables finished");

            Log.info("Tables creation stated");
            String createTeam = "CREATE TABLE IF NOT EXISTS TEAM " +
                    "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    " name TEXT NOT NULL)";

            String createSkill = "CREATE TABLE IF NOT EXISTS SKILL " +
                    "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    " name TEXT NOT NULL)";

            String createManager = "CREATE TABLE IF NOT EXISTS MANAGER " +
                    "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    " name TEXT NOT NULL," +
                    " firstname TEXT NOT NULL," +
                    " mail TEXT NOT NULL," +
                    " password TEXT NOT NULL," +
                    " picturepath TEXT NOT NULL)";

            String createManagerTeam = "CREATE TABLE IF NOT EXISTS MANAGERTEAM " +
                    "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    " managerid INT NOT NULL," +
                    " teamid INT NOT NULL," +
                    " FOREIGN KEY(managerId) REFERENCES MANAGER(id)," +
                    " FOREIGN KEY(teamId) REFERENCES TEAM(id))";

            String createPilot = "CREATE TABLE IF NOT EXISTS PILOT " +
                    "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    " name TEXT NOT NULL," +
                    " firstname TEXT NOT NULL," +
                    " mail TEXT NOT NULL," +
                    " password TEXT NOT NULL," +
                    " picturepath TEXT," +
                    " teamid INT NOT NULL," +
                    " FOREIGN KEY (teamid) REFERENCES TEAM(id))";

            String createPilotSkill = "CREATE TABLE IF NOT EXISTS PILOTSKILL " +
                    "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    " startDate DATE NOT NULL," +
                    " endDate DATE NOT NULL," +
                    " pilotid INT NOT NULL," +
                    " skillid INT NOT NULL," +
                    " FOREIGN KEY (pilotid) REFERENCES PILOT(id)," +
                    " FOREIGN KEY (skillid) REFERENCES SKILL(id))";

            String createMission = "CREATE TABLE IF NOT EXISTS MISSION " +
                    "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    " name TEXT NOT NULL," +
                    " startDate DATE NOT NULL," +
                    " endDate DATE NOT NULL," +
                    " state INT NOT NULL)";

            String createPlane = "CREATE TABLE IF NOT EXISTS PLANE " +
                    "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    " name TEXT NOT NULL," +
                    " type INT NOT NULL," +
                    " buydate DATE NOT NULL," +
                    " revisiondate DATE NOT NULL)";

            String createMissionPilotPlane = "CREATE TABLE IF NOT EXISTS MISSIONPILOTPLANE " +
                    "(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    " missionid INT NOT NULL," +
                    " pilotid INT NOT NULL," +
                    " planeid INT NOT NULL," +
                    " FOREIGN KEY (missionid) REFERENCES MISSON(id)," +
                    " FOREIGN KEY (pilotid) REFERENCES PILOT(id)," +
                    " FOREIGN KEY (planeid) REFERENCES PLANE(id))";

            stmt.executeUpdate(createTeam);
            stmt.executeUpdate(createSkill);
            stmt.executeUpdate(createManager);
            stmt.executeUpdate(createManagerTeam);
            stmt.executeUpdate(createPilot);
            stmt.executeUpdate(createPilotSkill);
            stmt.executeUpdate(createMission);
            stmt.executeUpdate(createPlane);
            stmt.executeUpdate(createMissionPilotPlane);
            Log.info("Tables creation finished");

            c.commit();

            stmt.close();
            c.close();

            Log.info("Mock data generation");
            for (int i = 0; i < 10; i++) {
                Team t = new Team();
                t.setName("Team n°" + i);
                TeamDao.createTeam(t);
            }

            for (int i = 0; i < 50; i++) {
                Pilot pilot = new Pilot();
                pilot.setTeam(TeamDao.getTeamById(i % 10 + 1));
                pilot.setName("nom" + i);
                pilot.setFirstname("prénom" + i);
                pilot.setMail(i + "pilot@pilot.fr");
                pilot.setPassword("toto");
                pilot.setPicturePath("/path" + i);
                UserDao.createPilot(pilot);
            }

            Pilot pilot = new Pilot();
            pilot.setTeam(TeamDao.getTeamById(1));
            pilot.setName("nom");
            pilot.setFirstname("prénom");
            pilot.setMail("admin@acu.fr");
            pilot.setPassword("toto");
            pilot.setPicturePath("/path");

            UserDao.createPilot(pilot);

            for (int i = 0; i < 5; i++) {
                Manager manager = new Manager();

                List<Team> teamList = new ArrayList<>();
                teamList.add(TeamDao.getTeamById(i / 2 + 1));
                teamList.add(TeamDao.getTeamById((i + 1) / 2 + 1));

                manager.setTeams(teamList);
                manager.setName("nom_manager" + i);
                manager.setFirstname("prénom_manager" + i);
                manager.setMail(i + "manager@manager.fr");
                manager.setPassword("toto");
                manager.setPicturePath("/path" + i);
                UserDao.createManager(manager);
            }

            for (int i = 0; i < 30; i++) {
                Plane plane = new Plane();
                plane.setName("plane" + i);
                plane.setType(Plane.PlaneType.fromInt(i % 5));
                plane.setBuyDate(new Date(Calendar.getInstance().getTime().getTime() + i * 34000000));
                plane.setLastCheckDate(new Date(Calendar.getInstance().getTime().getTime() + i * 24000000));
                PlaneDao.createPlane(plane);
            }

            for (int i = 0; i < 100; i++) {
                Mission mission = new Mission();
                mission.setName("mission no°" + i);
                mission.setStartDate(new Date(Calendar.getInstance().getTime().getTime() + i * 25000000));
                mission.setEndDate(new Date(Calendar.getInstance().getTime().getTime() + i * 26000000));
                mission.setMissonState(Mission.MissonState.fromInt(i % 5));
                MissionDao.createMission(mission);
            }

            for (int i = 0; i < 50; i++) {
                Skill s = new Skill();
                s.setName("Skill no°" + i);
                SkillDao.createSkill(s);
            }

            Log.info("Mock data inserted");
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        isDbInit = true;
    }
}
