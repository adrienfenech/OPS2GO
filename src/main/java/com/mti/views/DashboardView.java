package com.mti.views;

import com.mti.utils.Page;
import com.mti.utils.Tools;
import com.vaadin.cdi.CDIView;
import com.vaadin.data.Item;
import com.vaadin.ui.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Date;
import java.util.Random;

@CDIView("dashboard")
public class DashboardView extends AbstractView {
    private static final Logger Log = LogManager.getLogger(DashboardView.class);
    public static final String NAME = "dashboard";

    public DashboardView()
    {
        super("Tableau de bord", Page.DASHBOARD);
    }

    @Override
    protected void buildContent()
    {
        // Initialize components
        Component notifications = buildNotifs();
        Component missions = buildMissions();

        HorizontalLayout innerLayout = new HorizontalLayout();
        innerLayout.addComponent(notifications);
        innerLayout.addComponent(missions);
        innerLayout.setSpacing(true);

        contentLayout.addComponent(innerLayout);
    }

    private Component buildNotifs()
    {
        Table table = new Table();

        // Define column headers
        table.addContainerProperty("Statut", String.class, null);
        table.addContainerProperty("Message", String.class, null);

        // Define size
        table.setWidth(100f, Unit.PERCENTAGE);
        table.setPageLength(5);

        // Override cell style to display accurate colors depending on the message's type.
        table.setCellStyleGenerator((source, itemId, propertyId) -> {
            if (propertyId != null) {
                Item item = source.getItem(itemId);
                if (item.getItemProperty(propertyId).getValue().getClass() == String.class) {
                    String cellValue = (String) item.getItemProperty(propertyId).getValue();
                    if (cellValue.equals("ALERT"))
                        return "red";
                    else if (cellValue.equals("WARNING"))
                        return "orange";
                    else if (cellValue.equals("INFO"))
                        return "green";
                    else
                        return "white";
                } else {
                    return "white";
                }
            } else {
                return null;
            }
        });

        // Add mock messages to the Notifications table.
        mockMessages(table, 5);

        // Insert the messages' table into a panel.
        Panel panel = new Panel("Notifications");
        panel.setContent(table);

        return panel;
    }

    private Component buildMissions()
    {
        final int NB_ROWS = 3;

        Panel past = new Panel("Missions précédentes");
        Table pastMissions = new Table();
        pastMissions.addContainerProperty("Début", Date.class, null);
        pastMissions.addContainerProperty("Code", String.class, null);
        pastMissions.addContainerProperty("Pilote", String.class, null);
        pastMissions.setPageLength(NB_ROWS);
        for (int i = 0; i < NB_ROWS; i++)
            pastMissions.addItem(mockMission(), i);
        past.setContent(pastMissions);

        Panel current = new Panel("Missions en cours");
        Table currentMissions = new Table();
        currentMissions.addContainerProperty("Début", Date.class, null);
        currentMissions.addContainerProperty("Code", String.class, null);
        currentMissions.addContainerProperty("Pilote", String.class, null);
        currentMissions.setPageLength(NB_ROWS);
        for (int i = 0; i < NB_ROWS; i++)
            currentMissions.addItem(mockMission(), i);
        current.setContent(currentMissions);

        Panel next = new Panel("Missions futures");
        Table nextMissions = new Table();
        nextMissions.addContainerProperty("Début", Date.class, null);
        nextMissions.addContainerProperty("Code", String.class, null);
        nextMissions.addContainerProperty("Pilote", String.class, null);
        nextMissions.setPageLength(NB_ROWS);
        for (int i = 0; i < NB_ROWS; i++)
            nextMissions.addItem(mockMission(), i);
        next.setContent(nextMissions);

        VerticalLayout layout = new VerticalLayout();
        layout.addComponent(past);
        layout.addComponent(current);
        layout.addComponent(next);
        layout.setSpacing(true);

        return layout;
    }

    private Object[] mockMission()
    {
        String code = Tools.generateString().substring(0, 10).toUpperCase();
        Date date = new Date();
        Random r = new Random();
        String pilot = (r.nextInt() % 2 == 0) ? "Connor Keating" : "Dave Okumu";

        return new Object[] { date, code, pilot };
    }

    private void mockMessages(Table table, int rows)
    {
        Random r = new Random();

        for (int i = 0; i < rows; i++)
        {
            String type;
            String message;

            switch (r.nextInt() % 3)
            {
                case 0:
                    type = "ALERT";
                    message = "La mission CZK-1281AB a été annulée par le pilote.";
                    break;
                case 1:
                    type = "WARNING";
                    message = "La mission TPA-9914BZ n'a toujours pas de pilote 48h avant.";
                    break;
                default:
                    type = "INFO";
                    message = "La mission IEE-1022JD a eu un retard de 30 minutes.";
                    break;
            }

            table.addItem(new Object[] { type, message }, i);
        }
    }
}
