package com.mti.views;

import com.mti.dao.UserDao;
import com.mti.model.User;
import com.vaadin.cdi.CDIView;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@CDIView("login")
public class LoginView extends CustomComponent implements View, Button.ClickListener {

    private static final Logger Log = LogManager.getLogger(LoginView.class);

    public static final String NAME = "login";
    private TextField email;
    private PasswordField password;
    private Button signin;

    public LoginView() {
        setSizeFull();
        setCompositionRoot(buildContent());
    }

    private Component buildContent() {
        // Fetch logo
        Image img = new Image("", new ThemeResource("img/logo.png"));
        img.setWidth(530, Unit.PIXELS);

        // Create panel
        Panel panel = (Panel) buildPanel();

        // Initialize layout
        final VerticalLayout contentLayout = new VerticalLayout();
        contentLayout.addComponent(img);
        contentLayout.setComponentAlignment(img, Alignment.MIDDLE_CENTER);
        contentLayout.addComponent(panel);
        contentLayout.setComponentAlignment(panel, Alignment.MIDDLE_CENTER);

        final VerticalLayout layout = new VerticalLayout();
        layout.addComponent(contentLayout);
        layout.setComponentAlignment(contentLayout, Alignment.MIDDLE_CENTER);
        layout.setSizeFull();

        return layout;
    }

    private Component buildPanel() {
        final VerticalLayout panelLayout = new VerticalLayout();
        panelLayout.setWidth(525, Unit.PIXELS);
        panelLayout.setMargin(true);
        panelLayout.setSpacing(true);
        panelLayout.addComponent(buildTitle());
        panelLayout.addComponent(buildForm());

        Panel panel = new Panel();
        panel.setWidth(530, Unit.PIXELS);
        panel.setContent(panelLayout);

        return panel;
    }

    private Component buildTitle() {
        final HorizontalLayout titleLayout = new HorizontalLayout();
        Label welcome = new Label("Bienvenue sur l'application OPS2GO !");
        titleLayout.addComponent(welcome);
        return titleLayout;
    }

    private Component buildForm() {
        email = new TextField("Adresse e-mail");
        email.setInputPrompt("admin@adms.com");
        email.setIcon(FontAwesome.USER);
        email.setRequired(true);
        email.addValidator(new EmailValidator("L'adresse e-mail est invalide."));
        email.setInvalidAllowed(false);

        password = new PasswordField("Mot de passe");
        password.setIcon(FontAwesome.LOCK);
        password.setInputPrompt("********");
        password.setRequired(true);
        password.addValidator(new StringLengthValidator("Mot de passe erroné", 2, 50, false));
        password.setValue("");
        password.setNullRepresentation("");

        signin = new Button("Connexion");
        signin.addClickListener(this);

        final HorizontalLayout formsLayout = new HorizontalLayout();
        formsLayout.addComponent(email);
        formsLayout.addComponent(password);
        formsLayout.addComponent(signin);
        formsLayout.setComponentAlignment(signin, Alignment.BOTTOM_RIGHT);
        formsLayout.setSpacing(true);

        return formsLayout;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        email.focus();
    }

    @Override
    public void buttonClick(Button.ClickEvent clickEvent) {
        try {
            // Basic field validation
            email.validate();
            password.validate();

            // Fetch input values
            String email = this.email.getValue();
            String password = this.password.getValue();

            User user = UserDao.connect(email, password);

            if (user != null) {
                // Store user object in VaadinSession
                getSession().setAttribute("user", user);

                // Navigate to main view
                getUI().getNavigator().navigateTo(DashboardView.NAME);
            } else {
                // Wrong password clear the password field and refocuses it
                this.password.setValue(null);
                this.password.focus();
                Notification.show("Erreur d'identification : vérifiez les données entrées.");
            }
        } catch (Validator.InvalidValueException ex) {
            final String exceptionString = ex.getMessage();
            if (exceptionString.equals(""))
                Notification.show("Le(s) champ(s) ne peuvent être vides.");
            else
                Notification.show(exceptionString);
        }
    }
}
