package com.mti.views;

import com.mti.model.Manager;
import com.mti.model.User;
import com.mti.utils.Page;
import com.mti.utils.Tools;
import com.vaadin.cdi.CDIView;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@CDIView("sidebar")
public class MenuView extends CustomComponent implements View {

    private static final Logger Log = LogManager.getLogger(MenuView.class);

    public MenuView() {
    }

    public MenuView(final User user, final Page currentView) {
        setCompositionRoot(buildContent(user, currentView));
    }

    private Component buildContent(final User user, final Page currentView) {
        Layout userLayout = (Layout) buildUser(user);
        Layout itemsLayout = (Layout) buildMenuItems(currentView);

        VerticalLayout layout = new VerticalLayout();
        layout.setSpacing(true);
        layout.addComponent(userLayout);
        userLayout.setHeight(40, Unit.PERCENTAGE);
        layout.addComponent(itemsLayout);
        itemsLayout.setHeight(60, Unit.PERCENTAGE);

        VerticalLayout globalLayout = new VerticalLayout();
        globalLayout.setStyleName("sidebar");
        globalLayout.addComponent(layout);
        globalLayout.setSizeFull();

        return globalLayout;
    }

    private Component buildUser(final User user) {
        Image avatar = new Image("", new ThemeResource("img/pilot.jpg"));
        avatar.addStyleName("center");

        Label name = new Label(String.format("%s %s", user.getFirstname(), user.getName()));
        name.addStyleName("center");

        String title = (user instanceof Manager) ? "Manager" : "Pilote";
        Label role = new Label(String.format("<strong>%s</strong>", title), ContentMode.HTML);
        role.addStyleName("center");

        VerticalLayout layout = new VerticalLayout();
        layout.addComponent(avatar);
        layout.addComponent(name);
        layout.addComponent(role);
        layout.setSpacing(true);
        return layout;
    }

    private Component buildMenuItems(final Page currentView)
    {
        VerticalLayout layout = new VerticalLayout();

        layout.addComponent(buildMenuItem("Tableau de bord", FontAwesome.HOME, DashboardView.NAME, currentView));
        layout.addComponent(buildMenuItem("Missions", FontAwesome.PLANE, DashboardView.NAME, currentView));
        layout.addComponent(buildMenuItem("Pilote", FontAwesome.USERS, DashboardView.NAME, currentView));
        layout.addComponent(buildMenuItem("Profil", FontAwesome.USER_MD, DashboardView.NAME, currentView));
        layout.addComponent(buildMenuItem("Déconnexion", FontAwesome.SIGN_OUT, DashboardView.NAME, currentView));
        return layout;
    }

    private Component buildMenuItem(String title, FontAwesome icon, final String target, final Page currentView)
    {
        Button b = new Button(title);
        b.setIcon(icon);
        b.setStyleName("sidebar-button");
        b.setWidth(100, Unit.PERCENTAGE);
        b.addClickListener(clickEvent -> getUI().getNavigator().navigateTo(target));

        if (Tools.stringToPage(title) == currentView)
            b.addStyleName("sidebar-button-selected");

        return b;
    }


    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
    }
}
