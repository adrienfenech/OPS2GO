package com.mti.views;

import com.mti.model.User;
import com.mti.utils.Page;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;

/**
 * Base view that initializes the sidebar (with the appropriate highlight) and the page's title.
 */
public abstract class AbstractView extends CustomComponent implements View {

    protected Panel panel;
    protected VerticalLayout contentLayout;
    protected String title;

    public AbstractView(final String title, final Page menuHighlight)
    {
        this.title = title;
        setSizeFull();

        // Initialize sidebar
        User user = (User) VaadinSession.getCurrent().getAttribute("user");
        MenuView menu = new MenuView(user, menuHighlight);
        menu.setWidth(200, Unit.PIXELS);
        menu.setHeight(100, Unit.PERCENTAGE);

        // Initialize content layout with the page's title.
        contentLayout = new VerticalLayout();
        contentLayout.addComponent(new Label(String.format("<h1>%s</h1>", title), ContentMode.HTML));
        buildContent();

        // Scrollable container that contains the page's content.
        panel = new Panel();
        panel.setContent(contentLayout);
        panel.setSizeFull();
        panel.setStyleName("content");

        // Initial layout that holds the sidebar and the scrollable panel.
        HorizontalLayout layout = new HorizontalLayout();
        layout.addComponent(menu);
        layout.addComponent(panel);
        layout.setSpacing(true);
        layout.setSizeFull();
        layout.setExpandRatio(panel, 1f);

        setCompositionRoot(layout);
    }

    protected abstract void buildContent();

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }
}
