package com.mti.dao;

import com.mti.Database;
import com.mti.model.Plane;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by Jimmy on 23/11/15.
 */
public class PlaneDao {
    public static void createPlane(Plane p) throws Exception {
        Connection c =  Database.getConnection();
        Statement stmt = c.createStatement();
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        String values = String.format("VALUES ('%s','%d','%s','%s')",
                p.getName(), p.getType().getValue(), dateFormat.format(p.getBuyDate()), dateFormat.format(p.getLastCheckDate()));

        String sql = "INSERT INTO PLANE (name, type, buydate, revisiondate)" +  values;

        stmt.executeUpdate(sql);
        ResultSet rs = stmt.getGeneratedKeys();
        rs.next();
        p.setId(rs.getInt(1));

        stmt.close();
        c.commit();
        c.close();
    }
}
