package com.mti.dao;

import com.mti.Database;
import com.mti.model.Team;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jimmy on 22/11/15.
 */
public class TeamDao {


    private static final Logger Log = LogManager.getLogger(TeamDao.class);

    public static List<Team> getAllTeams() {
        List<Team> teamList = new ArrayList<>();
        try {
            Connection c = Database.getConnection();
            Statement stmt;

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM TEAM;");
            while (rs.next()) {
                Team team = new Team();

                team.setId(rs.getInt("id"));
                team.setName(rs.getString("name"));
                teamList.add(team);
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (Exception e) {
        }

        return teamList;
    }

    public static List<Team> getManagerTeams(int managerId) {
        List<Team> teamList = new ArrayList<>();
        try {
            Connection c = Database.getConnection();
            Statement stmt;

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM TEAM WHERE TEAM.manager_id = " + managerId);
            while (rs.next()) {
                Team team = new Team();

                team.setId(rs.getInt("id"));
                team.setName(rs.getString("name"));
                teamList.add(team);
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (Exception e) {
        }

        return teamList;
    }

    public static Team getTeamById(int teamId) {
        try {
            Team team = null;
            Connection c = Database.getConnection();
            Statement stmt;

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM TEAM WHERE id = " + teamId);

            if (rs.next()) {
                team = new Team();

                team.setId(rs.getInt("id"));
                team.setName(rs.getString("name"));
            }
            rs.close();
            stmt.close();
            c.close();
            return team;
        } catch (Exception e) {
            return null;
        }
    }

    public static void createTeam(Team team) throws Exception {
        Connection c =  Database.getConnection();
        Statement stmt = c.createStatement();

        String sql = "INSERT INTO TEAM (name) VALUES ('" + team.getName() + "')";

        stmt.executeUpdate(sql);
        ResultSet rs = stmt.getGeneratedKeys();
        rs.next();
        team.setId(rs.getInt(1));
        stmt.close();
        c.commit();
        c.close();
    }
}
