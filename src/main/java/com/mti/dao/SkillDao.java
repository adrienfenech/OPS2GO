package com.mti.dao;

import com.mti.Database;
import com.mti.model.Skill;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jimmy on 23/11/15.
 */
public class SkillDao {
    public static void createSkill(Skill skill) throws Exception {
        Connection c =  Database.getConnection();
        Statement stmt = c.createStatement();

        String sql = "INSERT INTO SKILL (name) VALUES ('" + skill.getName() + "')";

        stmt.executeUpdate(sql);
        ResultSet rs = stmt.getGeneratedKeys();
        rs.next();
        skill.setId(rs.getInt(1));
        stmt.close();
        c.commit();
        c.close();
    }

    public static List<Skill> getSkills() {
        try {
            List<Skill> skillList = new ArrayList<>();
            Connection c = Database.getConnection();
            Statement stmt;

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM SKILL");

            while (rs.next()) {
                Skill skill = new Skill();

                skill.setId(rs.getInt("id"));
                skill.setName(rs.getString("name"));

                skillList.add(skill);
            }
            rs.close();
            stmt.close();
            c.close();
            return skillList;
        } catch (Exception e) {
            return null;
        }
    }
}
