package com.mti.dao;

import com.mti.Database;
import com.mti.model.Manager;
import com.mti.model.Pilot;
import com.mti.model.Team;
import com.mti.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Jimmy on 22/11/15.
 */
public class UserDao {


    private static final Logger Log = LogManager.getLogger(UserDao.class);

    /**
     *
     * Try to find a user
     *
     * @param login user's mail
     * @param pass user's pass
     * @return the user, null otherwise
     */
    public static User connect(String login, String pass) {
        try {
            User userToReturn = null;
            Connection c = Database.getConnection();
            Statement stmt = c.createStatement();
            String connectQuery = "SELECT * FROM '%s' WHERE mail = '%s' and password = '%s'";
            ResultSet rs = stmt.executeQuery(String.format(connectQuery, "PILOT", login, pass));

            if (rs.next()) {
                userToReturn = pilotFromResultSet(rs);
            } else {
                rs.close();
                rs = stmt.executeQuery(String.format(connectQuery, "MANAGER", login, pass));
                if (rs.next()) {
                    userToReturn = managerFromResultSet(rs);
                }
            }
            rs.close();
            stmt.close();
            c.close();
            return userToReturn;
        } catch (Exception e) {
            return null;
        }

    }

    public static Pilot pilotFromResultSet(ResultSet rs) throws SQLException {
        Pilot p = new Pilot();
        setResultToUser(p, rs);

        p.setTeam(TeamDao.getTeamById(rs.getInt("teamid")));
        return p;
    }

    public static Manager managerFromResultSet(ResultSet rs) throws SQLException {
        Manager m = new Manager();
        setResultToUser(m, rs);

        m.setTeams(TeamDao.getManagerTeams(m.getId()));
        return m;
    }

    private static void setResultToUser(User user, ResultSet rs) throws SQLException {
        user.setId(rs.getInt("id"));
        user.setName(rs.getString("name"));
        user.setFirstname(rs.getString("firstname"));
        user.setMail(rs.getString("mail"));
        user.setPicturePath(rs.getString("picturepath"));
    }

    public static void createManager(Manager m) throws Exception {
        Connection c =  Database.getConnection();
        Statement stmt = c.createStatement();
        String values = String.format("VALUES ('%s','%s','%s','%s','%s')",
                m.getName(), m.getFirstname(), m.getMail(), m.getPassword(), m.getPicturePath());

        String sql = "INSERT INTO MANAGER (name,firstname,mail,password,picturepath) " +  values;

        stmt.executeUpdate(sql);
        ResultSet rs = stmt.getGeneratedKeys();
        rs.next();
        m.setId(rs.getInt(1));

        for (Team t : m.getTeams()) {
            sql = "INSERT INTO MANAGERTEAM (managerid, teamid) VALUES (" + m.getId() + ", " + t.getId() + ")";
            stmt.executeUpdate(sql);
        }

        stmt.close();
        c.commit();
        c.close();
    }

    public static void createPilot(Pilot p) throws Exception {
        Connection c =  Database.getConnection();
        Statement stmt = c.createStatement();
        String values = String.format("VALUES ('%s','%s','%s','%s','%s','%d')",
                p.getName(), p.getFirstname(), p.getMail(), p.getPassword(), p.getPicturePath(), p.getTeam().getId());

        String sql = "INSERT INTO PILOT (name,firstname,mail,password,picturepath,teamid) " +  values;

        stmt.executeUpdate(sql);
        ResultSet rs = stmt.getGeneratedKeys();
        rs.next();
        p.setId(rs.getInt(1));

        stmt.close();
        c.commit();
        c.close();
    }
}
