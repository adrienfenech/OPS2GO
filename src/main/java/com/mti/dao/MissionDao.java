package com.mti.dao;

import com.mti.Database;
import com.mti.model.Mission;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by Jimmy on 23/11/15.
 */
public class MissionDao {

    public static void createMission(Mission m) throws Exception {
        Connection c =  Database.getConnection();
        Statement stmt = c.createStatement();
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());

        String values = String.format("VALUES ('%s','%s','%s','%d')",
                m.getName(), dateFormat.format(m.getStartDate()), dateFormat.format(m.getEndDate()), m.getMissonState().getValue());

        String sql = "INSERT INTO MISSION (name, startDate, endDate, state) " + values;

        stmt.executeUpdate(sql);
        ResultSet rs = stmt.getGeneratedKeys();
        rs.next();
        m.setId(rs.getInt(1));
        stmt.close();
        c.commit();
        c.close();
    }
}
