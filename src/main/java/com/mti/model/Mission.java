package com.mti.model;

import lombok.Getter;
import lombok.Setter;

import java.sql.Date;


/**
 * Created by Jimmy on 22/11/15.
 */
@Getter
@Setter
public class Mission {
    int id;

    String name;
    Date startDate;
    Date endDate;
    MissonState missonState;

    public enum MissonState {
        PENDING(0),
        RUNNING(1),
        FINISH(2),
        CANCELED(3),
        ABORTED(4);

        private final int value;
        MissonState(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static MissonState fromInt(int i) {
            for (MissonState state : MissonState.values()) {
                if (state.getValue() == i)
                    return state;
            }
            return ABORTED;
        }

       public String getStateName() {
           switch (this) {
               case PENDING:
                   return "En attente";
               case RUNNING:
                   return "En cours";
               case FINISH:
                   return "Terminée";
               case CANCELED:
                   return "Annulée";
               case ABORTED:
                   return "Abandonnée";
               default:
                   return "État inconnu";
           }
       }
    }
}
