package com.mti.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by Jimmy on 22/11/15.
 */
@Getter
@Setter
public class Manager extends User {
    List<Team> teams;
}
