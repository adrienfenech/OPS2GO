package com.mti.model;

import lombok.Getter;
import lombok.Setter;

import java.sql.Date;

/**
 * Created by Jimmy on 22/11/15.
 */
@Getter
@Setter
public class Plane {
    int id;

    String name;
    PlaneType type;

    Date buyDate;
    Date lastCheckDate;

   public enum PlaneType {
        AAAAA(0),
        BBBBB(1),
        CCCCC(2),
        DDDDD(3),
        EEEEE(4),
        FFFFF(5);

        private final int value;
        PlaneType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static PlaneType fromInt(int i) {
            for (PlaneType type : PlaneType.values()) {
                if (type.getValue() == i)
                    return type;
            }
            return AAAAA;
        }

        public String getTypeName() {
            switch (this) {
                case AAAAA:
                    return "AAAAA";
                case BBBBB:
                    return "BBBBB";
                case CCCCC:
                    return "CCCCC";
                case DDDDD:
                    return "DDDDD";
                case EEEEE:
                    return "EEEEE";
                case FFFFF:
                    return "FFFFF";
                default:
                    return "Type inconnu";
            }
        }
    }
}
