package com.mti.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Jimmy on 22/11/15.
 */

@Getter
@Setter
public class Skill {
    int id;

    String name;
}
