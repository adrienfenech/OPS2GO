package com.mti.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by Jimmy on 22/11/15.
 */

@Getter
@Setter
public class PilotSkill {
    int id;

    Date startDate;
    Date endDate;

    Pilot pilot;
    Skill skill;
}
