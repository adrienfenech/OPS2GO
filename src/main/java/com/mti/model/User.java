package com.mti.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Jimmy on 22/11/15.
 */
@Getter
@Setter
public abstract class User {
    int id;

    String name;
    String firstname;
    String mail;
    String password;
    String picturePath;
}
