package com.mti;

import com.mti.views.DashboardView;
import com.mti.views.LoginView;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.cdi.CDIUI;
import com.vaadin.cdi.CDIViewProvider;
import com.vaadin.cdi.server.VaadinCDIServlet;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;

@Theme("mytheme")
@Widgetset("com.mti.MyAppWidgetset")
@CDIUI("")
public class MyUI extends UI {

    private static final Logger Log = LogManager.getLogger(MyUI.class);

    Navigator navigator;
    protected static final String MAINVIEW = "login";

    @Inject
    private CDIViewProvider viewProvider;

    public MyUI() {
        super();
        Database.initDb();
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        getPage().setTitle("OPS2GO");

        // Create navigator
        navigator = new Navigator(this, this);
        navigator.addProvider(viewProvider);

        // Handle view change
        navigator.addViewChangeListener(new ViewChangeListener() {
            @Override
            public boolean beforeViewChange(ViewChangeEvent event) {

                // Check if a user has logged in
                boolean isLoggedIn = getSession().getAttribute("user") != null;
                boolean isLoginView = event.getNewView() instanceof LoginView;

                if (!isLoggedIn && !isLoginView) {
                    getNavigator().navigateTo(LoginView.NAME);
                    return false;
                } else if (isLoggedIn && isLoginView){
                    getNavigator().navigateTo(DashboardView.NAME);
                    return false;
                }

                return true;
            }

            @Override
            public void afterViewChange(ViewChangeEvent viewChangeEvent) {

            }   
        });

        // Navigate to login page
        navigator.navigateTo(LoginView.NAME);
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinCDIServlet
    {
    }
}
